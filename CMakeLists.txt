cmake_minimum_required(VERSION 3.5)
project(godot)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

include_directories(./godot
                    ./godot/core/
                    ./godot/core/math/)

file (GLOB CPP_FILES godot/**/*.cpp)
file (GLOB H_FILES godot/**/*.h)
set (SOURCE_FILES ${CPP_FILES} ${H_FILES})

add_executable(godot ${SOURCE_FILES})
