//
// Created by jeremy on 9/22/16.
//

#include "register_types.h"
#include "object_type_db.h"
#include "player_controller_2d.h"

void register_player_controller_2d_types() {

    ObjectTypeDB::register_type<PlayerController2D>();
}

void unregister_player_controller_2d_types() {}
