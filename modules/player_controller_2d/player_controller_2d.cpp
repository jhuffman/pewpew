//
// Created by jeremy on 9/22/16.
//

#include "player_controller_2d.h"
#include "object_type_db.h"
#include "core/os/os.h"
#include "scene/2d/node_2d.h"

PlayerController2D::PlayerController2D() : input(*Input::get_singleton()) {

    set_player_number("1");
}

void PlayerController2D::_bind_methods() {

    ObjectTypeDB::bind_method(_MD("set_player_speed","speed"), &PlayerController2D::set_player_speed);
    ObjectTypeDB::bind_method(_MD("get_player_speed"),&PlayerController2D::get_player_speed);
    ADD_PROPERTYNO(PropertyInfo( Variant::INT, "player_speed",PROPERTY_HINT_RANGE,"1,500,10"), _SCS("set_player_speed"),_SCS("get_player_speed"));

    ObjectTypeDB::bind_method(_MD("set_player_number","number"), &PlayerController2D::set_player_number);
    ObjectTypeDB::bind_method(_MD("get_player_number"),&PlayerController2D::get_player_number);
    ADD_PROPERTY(PropertyInfo( Variant::STRING, "player_number"), _SCS("set_player_number"),_SCS("get_player_number"));
}

void PlayerController2D::_notification(int p_notification) {

    switch (p_notification) {
        case NOTIFICATION_READY:
            if (!get_tree()->is_editor_hint()) {
                set_fixed_process(true);
            }
            break;
        case NOTIFICATION_EXIT_TREE: {
            set_fixed_process(false);
            break;
        }
        case NOTIFICATION_FIXED_PROCESS:
            Array profiling;
            uint64_t time_beg = OS::get_singleton()->get_ticks_usec();

            process_movement(get_fixed_process_delta_time());

            ScriptDebugger* debugger = ScriptDebugger::get_singleton();
            if (debugger) {
                profiling.push_back("process_movement");
                profiling.push_back(USEC_TO_SEC(OS::get_singleton()->get_ticks_usec()-time_beg));
                debugger->add_profiling_frame_data("player_controller_2d",profiling);
            }

            break;
        }
}

void PlayerController2D::process_movement(float delta) {

    Node2D* parent = dynamic_cast<Node2D*>(get_parent());
    if (!parent) { return; }
    Vector2 rot;
    int keys = 0;

    if (input.is_action_pressed(key_up)) {
        rot.y -= 1;
        keys++;
    }
    if (input.is_action_pressed(key_down)) {
        rot.y += 1;
        keys++;
    }
    if (input.is_action_pressed(key_right)) {
        rot.x += 1;
        keys++;
    }
    if (input.is_action_pressed(key_left)) {
        rot.x -= 1;
        keys++;
    }

    int speed = player_speed;
    if (keys == 2) {
        speed = sqrt((speed * speed) / 2);
    }
    parent->set_rot((-rot).angle());
    parent->set_pos(parent->get_pos() + (rot * (speed * delta)));
}

int PlayerController2D::get_player_speed() const {

    return player_speed;
}

void PlayerController2D::set_player_speed(int p_speed) {

    player_speed = p_speed;
}

const String& PlayerController2D::get_player_number() {

    return player_number;
}

void PlayerController2D::set_player_number(const String& p_number) {

    player_number = p_number;
    key_up = "player_" + player_number + "_up";
    key_down= "player_" + player_number + "_down";
    key_left = "player_" + player_number + "_left";
    key_right = "player_" + player_number + "_right";
}
