//
// Created by jeremy on 9/22/16.
//

#ifndef GODOT_PLAYER_CONTROLLER_2D_H
#define GODOT_PLAYER_CONTROLLER_2D_H

#include "scene/main/node.h"
#include "core/os/input.h"

class PlayerController2D : public Node {
    OBJ_TYPE(PlayerController2D, Node);
    int player_speed;
    String player_number;
    StringName key_up;
    StringName key_down;
    StringName key_left;
    StringName key_right;

    Input& input;

    void process_movement(float delta);

protected:
    void _notification(int p_notification);
    static void _bind_methods();

public:
    PlayerController2D();
    int get_player_speed() const;
    void set_player_speed(int p_speed);

    const String& get_player_number();
    void set_player_number(const String& p_number);
};

#endif //GODOT_PLAYER_CONTROLLER_2D_H
