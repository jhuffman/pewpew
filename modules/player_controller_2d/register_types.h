//
// Created by jeremy on 9/22/16.
//

#ifndef GODOT_REGISTER_TYPES_H
#define GODOT_REGISTER_TYPES_H

void register_player_controller_2d_types();
void unregister_player_controller_2d_types();

#endif //GODOT_REGISTER_TYPES_H
