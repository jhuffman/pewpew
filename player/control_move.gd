extends Node

export var player_speed = 300.0
export var player_number = "1" setget player_number_set

#cache input action names so we don't build strings in every frame
onready var key_up = key_name("up")
onready var key_down = key_name("down")
onready var key_right = key_name("right")
onready var key_left = key_name("left")

func key_name(direction):
	return "player_" + player_number + "_" + direction

func player_number_set(new_number):
	player_number = new_number
	key_up = key_name("up")
	key_down = key_name("down")
	key_right = key_name("right")
	key_left = key_name("left")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	process_movement(delta)

func process_movement(delta):
	var rot = Vector2(0, 0)
	var keys = 0
	var stick

	if Input.is_action_pressed(key_up):
		stick = stick_event(key_up)
		rot.y -= 1
		keys +=1
	if Input.is_action_pressed(key_down):
		stick = stick_event(key_down)
		rot.y += 1
		keys +=1
	if Input.is_action_pressed(key_right):
		stick = stick_event(key_right)
		rot.x += 1
		keys +=1
	if Input.is_action_pressed(key_left):
		stick = stick_event(key_left)
		rot.x -= 1
		keys +=1

	if stick != null:
		var x = Input.get_joy_axis(stick.device, JOY_AXIS_0)
		var y = Input.get_joy_axis(stick.device, JOY_AXIS_1)
		if (x > .10 || x < -.10 || y > .10 || y < -.10):
			rot.x = x
			rot.y = y


	var speed = player_speed
	if keys == 2:
		speed = sqrt((player_speed * player_speed) / 2)

	var p = get_parent()
	p.set_rot((-rot).angle())
	p.set_pos(p.get_pos() + (rot * (speed * delta)))

func stick_event(name):
	var list = InputMap.get_action_list(name)
	for ev in list:
		if ev.type == InputEvent.JOYSTICK_MOTION:
			return ev
	return null